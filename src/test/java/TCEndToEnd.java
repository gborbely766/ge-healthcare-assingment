import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.http.Method;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.path.json.JsonPath;


public class TCEndToEnd {

    private String token;
    private Integer id;

    @Test(priority = 1)
    public void getToken(){
        RestAssured.baseURI = "https://restful-booker.herokuapp.com/auth";
        JSONObject requestParams = new JSONObject();
        requestParams.put("username", "admin");
        requestParams.put("password", "password123");
        RequestSpecification request = RestAssured.given()
                .header("Content-Type", "application/json")
                .body(requestParams.toJSONString());
        Response response = request.request(Method.POST);
        JsonPath path = response.jsonPath();
        token = path.get("token");
        String responseAsString = response.asString();
        System.out.println("getToken() Response: " + response.getStatusCode());
        response.print();
        Assert.assertTrue(responseAsString.contains("token"));
        Assert.assertEquals(response.statusCode(), 200);
    }

    @Test(priority = 2)
    public void createNewBooking(){
        Response response = RestAssured.given().baseUri("https://restful-booker.herokuapp.com/booking")
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "\"firstname\" : \"Gyula\",\n" +
                        "\"lastname\" : \"Borbely\",\n" +
                        "\"totalprice\" : 110,\n" +
                        "\"depositpaid\" : true,\n" +
                        "\"bookingdates\" : {\n" +
                        "    \"checkin\" : \"2019-01-01\",\n" +
                        "    \"checkout\" : \"2020-01-01\"\n" +
                        "},\n" +
                        "\"additionalneeds\" : \"Breakfast\"\n" +
                        "}")
                .when().post();
        JsonPath jsonBody = response.jsonPath();
        id = jsonBody.get("bookingid");
        String responseAsString = response.asString();
        System.out.println("createNewBooking() response: " + response.getStatusCode());
        response.print();
        Assert.assertEquals(response.statusCode(),200);
        Assert.assertTrue(responseAsString.contains("Gyula"));
    }

    @Test(priority = 3)
    public void getNewEntry(){
        Response response = RestAssured.given().baseUri("https://restful-booker.herokuapp.com/booking")
                .basePath(id.toString())
                .when().get();
        JsonPath jsonBody = response.jsonPath();
        System.out.println("getNewEntry response: " + response.getStatusCode());
        response.print();
        Assert.assertEquals(response.statusCode(),200);
        Assert.assertEquals(jsonBody.get("lastname"), "Borbely");
    }

    @Test(priority = 4)
    public void updateEntry(){
        Response response = RestAssured.given().baseUri("https://restful-booker.herokuapp.com/booking")
                .basePath(id.toString())
                .contentType(ContentType.JSON)
                .accept("application/json")
                .auth().preemptive().basic("admin", "password123")
                .body("{\n" +
                        "\"firstname\" : \"Gyula\",\n" +
                        "\"lastname\" : \"Borbely\",\n" +
                        "\"totalprice\" : 150,\n" +
                        "\"depositpaid\" : false,\n" +
                        "\"bookingdates\" : {\n" +
                        "    \"checkin\" : \"2020-01-01\",\n" +
                        "    \"checkout\" : \"2021-01-01\"\n" +
                        "},\n" +
                        "\"additionalneeds\" : \"None\"\n" +
                        "}")
                .when().put();
        JsonPath jsonBody = response.jsonPath();
        String price = jsonBody.get("totalprice").toString();
        Boolean paid = jsonBody.get("depositpaid");
        String needs = jsonBody.get("additionalneeds");
        System.out.println("updateEntry() response: " + response.getStatusCode());
        response.print();
        Assert.assertEquals(response.statusCode(),200);
        Assert.assertEquals(price, "150");
        Assert.assertEquals(paid.booleanValue(), false);
        Assert.assertEquals(needs, "None");
    }

    @Test(priority = 5)
    public void deleteEntry(){
        Response response = RestAssured.given().baseUri("https://restful-booker.herokuapp.com/booking")
                .basePath(id.toString())
                .contentType(ContentType.JSON)
                .cookie("token", token)
                .when().delete();
        System.out.println("deleteEntry() response: " + response.getStatusCode());
        response.print();
        Assert.assertEquals(response.statusCode(),201);

    }

    @Test(priority = 6)
    public void checkEntryWasDeleted(){
        Response response = RestAssured.given().baseUri("https://restful-booker.herokuapp.com/booking")
                .basePath(id.toString())
                .when().get();
        String responseAsString = response.asString();
        System.out.println("checkEntry() response: " + response.getStatusCode());
        response.print();
        Assert.assertEquals(response.statusCode(), 404);
        Assert.assertTrue(responseAsString.contains("Not Found"));
    }

    @Test(priority = 7)
    public void tryAndFindIdInAllEntries(){
        Response response = RestAssured.given().baseUri("https://restful-booker.herokuapp.com/booking")
                .when().get();
        String responseAsString = response.asString();
        System.out.println("tryAndFindIdInAllEntries() response: " + response.getStatusCode());
        response.print();
        Assert.assertEquals(responseAsString.contains(id.toString()), false);
    }
}



