import Model.LoginModel;
import org.example.WebDriverService;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class LoginTest {
    private WebDriver webDriver;
    private LoginModel loginModel;

    @BeforeMethod
    private void setUp()  {
        this.webDriver = WebDriverService.init();
        loginModel = new LoginModel(webDriver);
        webDriver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");

    }

    @Test
    public void successfulLogin()  {
        loginModel.DoLogin();
        Assert.assertTrue(loginModel.isUserLoggedIn());
        webDriver.close();
    }

    @Test
    public void wrongUsername()  {
        loginModel.logInWithWrongUserName();
        Assert.assertTrue(loginModel.isErrorMsgPresent());

    }

    @Test
    public void wrongPassword()  {
        loginModel.logInWithWrongPassword();
        Assert.assertTrue(loginModel.isErrorMsgPresent());

    }

    @Test
    public void noCredentialsProvided(){
        loginModel.clickLoginButton();
        Assert.assertTrue(loginModel.isAlertMsgPresent());
    }

    @AfterMethod
    private void closeDriver(){
        WebDriverService.quitWebDriver();
    }
}
