import Model.LogOutModel;
import Model.LoginModel;
import org.example.WebDriverService;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class LogOutTest {

    private WebDriver webDriver;
    private LoginModel loginModel;
    private LogOutModel logOutModel;

    @BeforeMethod
    private void setUp() {
        this.webDriver = WebDriverService.init();
        loginModel = new LoginModel(webDriver);
        logOutModel = new LogOutModel(webDriver);
        webDriver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");

    }

    @Test
    public void SuccessfulLogOut(){
        loginModel.DoLogin();
        logOutModel.doLogout();
        String expected = "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login";
        Assert.assertEquals(logOutModel.getLoginPageURL(), expected);

    }

    @AfterMethod
    private void closeDriver(){
        WebDriverService.quitWebDriver();
    }
}
