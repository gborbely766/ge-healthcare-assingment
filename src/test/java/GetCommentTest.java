import Model.LoginModel;
import Model.TimeSheetModel;
import org.example.WebDriverService;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class GetCommentTest {
    private WebDriver webDriver;
    private LoginModel loginModel;
    private TimeSheetModel timeSheetModel;

    @BeforeMethod
    private void setUp()  {
        this.webDriver = WebDriverService.init();
        loginModel = new LoginModel(webDriver);
        timeSheetModel = new TimeSheetModel(webDriver);
        webDriver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");

    }
    @Test
    public void checkAndPrintComment(){
        loginModel.DoLogin();
        timeSheetModel.navigateToCommentModal();
        String comment = timeSheetModel.getTextFromCommentModal();
        System.out.println(comment);
        Assert.assertEquals(comment, "Leadership Development");
    }

    @AfterMethod
    private void closeDriver(){
        WebDriverService.quitWebDriver();
    }
}
