package Model;

import org.example.WebDriverService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class LogOutModel {

    private final WebDriver webDriver;

    private WebDriverWait driverWait;

    @FindBy(xpath = "//*[@id=\"app\"]/div[1]/div[1]/header/div[1]/div[2]/ul/li")
    private WebElement profileMenuButton;

    @FindBy(xpath = "//*[@id=\"app\"]/div[1]/div[1]/header/div[1]/div[2]/ul/li/ul/li[4]/a")
    private WebElement logOutButton;

    public LogOutModel(WebDriver webDriver) {
        this.webDriver = webDriver;
        this.driverWait = new WebDriverWait(webDriver, Duration.ofSeconds(10));
        PageFactory.initElements(webDriver, this);
    }
    public void clickProfileMenuButton(){
        driverWait.until(ExpectedConditions.visibilityOf(profileMenuButton)).click();
    }

    public void clickLogOutButton(){
        driverWait.until(ExpectedConditions.visibilityOf(logOutButton)).click();
    }
    public String getLoginPageURL(){
        return webDriver.getCurrentUrl();
    }

    public void doLogout(){
        clickProfileMenuButton();
        clickLogOutButton();
    }

}
