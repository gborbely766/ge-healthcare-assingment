package Model;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public class LoginModel {

    private final WebDriver webDriver;

    private WebDriverWait driverWait;

    @FindBy(xpath = "//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[1]/div/div[2]/input")
    private WebElement usernameField;
    @FindBy(xpath = "//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[2]/div/div[2]/input")
    private WebElement passwordField;
    @FindBy(xpath = "//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[3]/button")
    private WebElement loginButton;

    @FindBy(xpath = "//*[@id=\"app\"]/div[1]/div[1]/header/div[1]/div[2]/ul/li/span/p")
    private WebElement profile;

    @FindBy(xpath = "//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/div/div[1]/div[1]/p")
    private WebElement errorMsg;

    @FindBy(xpath = "//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[1]/div/span")
    private WebElement alertMsg;


    public LoginModel(WebDriver driver)  {
        this.webDriver = driver;
        this.driverWait = new WebDriverWait(webDriver, Duration.ofSeconds(10));
        PageFactory.initElements(webDriver, this);
    }


    private void setUsername(){
        driverWait.until(ExpectedConditions.visibilityOf(usernameField)).sendKeys("Admin");
    }

    private void setPassword(){
        driverWait.until(ExpectedConditions.visibilityOf(passwordField)).sendKeys("admin123");
    }

    public void clickLoginButton(){
        driverWait.until(ExpectedConditions.visibilityOf(loginButton)).click();
    }

    private void setWrongUsername(){
        driverWait.until(ExpectedConditions.visibilityOf(usernameField)).sendKeys("gborbely");
    }
    private void setWrongPassword(){
        driverWait.until(ExpectedConditions.visibilityOf(passwordField)).sendKeys("gborbely123");
    }
    public void DoLogin(){
        setUsername();
        setPassword();
        clickLoginButton();
    }
    public void logInWithWrongUserName(){
        setWrongUsername();
        setPassword();
        clickLoginButton();
    }
    public void logInWithWrongPassword(){
        setUsername();
        setWrongPassword();
        clickLoginButton();
    }

    public boolean isUserLoggedIn(){
        try{
            return driverWait.until(ExpectedConditions.elementToBeClickable(profile)).isDisplayed();
        }catch (NoSuchElementException e){
            return false;
        }
    }
    public boolean isErrorMsgPresent(){
        try{
            return driverWait.until(ExpectedConditions.visibilityOf(errorMsg)).isDisplayed();
        }catch (NoSuchElementException e){
            return false;
        }
    }
    public boolean isAlertMsgPresent(){
        try{
            return driverWait.until(ExpectedConditions.visibilityOf(alertMsg)).isDisplayed();
        }catch (NoSuchElementException e){
            return false;
        }
    }

}
