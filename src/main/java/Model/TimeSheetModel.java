package Model;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


public class TimeSheetModel {
    private final WebDriver webDriver;

    private WebDriverWait driverWait;

    @FindBy(xpath = "//*[@id=\"app\"]/div[1]/div[1]/aside/nav/div[2]/ul/li[4]/a")
    private WebElement timeLink;

    @FindBy(xpath = "//*[text()='2022-08-15 - 2022-08-21']")
    private WebElement cardElement;

    @FindBy(xpath = "//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/form/div[2]/table/tbody/tr[1]/td[3]")
    private WebElement tableRowElement;

    @FindBy(xpath = "//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/form/div[4]/div/div/div/form/div[2]/div/div[2]/textarea")
    private WebElement commentModalTextArea;




    public TimeSheetModel(WebDriver driver) {
        this.webDriver = driver;
        this.driverWait = new WebDriverWait(webDriver, Duration.ofSeconds(10));
        PageFactory.initElements(webDriver, this);
    }

    private void clickOnTimeLink(){
        driverWait.until(ExpectedConditions.visibilityOf(timeLink)).click();
    }

    private void getTheCorrectTimeSheetAndClickOnItsViewButton() {
        driverWait.until(ExpectedConditions.visibilityOf(cardElement));
        WebElement parent = driverWait.until(ExpectedConditions.visibilityOf(cardElement.findElement(By.xpath(".."))));
        WebElement ancestor = driverWait.until(ExpectedConditions.visibilityOf(parent.findElement(By.xpath(".."))));
        WebElement button = driverWait.until(ExpectedConditions.visibilityOf(ancestor.findElement(By.xpath("..//button"))));
        button.click();
    }
    private void clickOnGreenLittleCommentButton(){
        driverWait.until(ExpectedConditions.visibilityOf(tableRowElement));
        WebElement greenButton = driverWait.until(ExpectedConditions.visibilityOf(tableRowElement.findElement(By.xpath("..//button"))));
        greenButton.click();

    }
    public String getTextFromCommentModal(){
        driverWait.until(ExpectedConditions.visibilityOf(commentModalTextArea));
        String text = commentModalTextArea.getAttribute("value");
        return text;
    }

    public void navigateToCommentModal(){
        clickOnTimeLink();
        getTheCorrectTimeSheetAndClickOnItsViewButton();
        clickOnGreenLittleCommentButton();
    }

}
