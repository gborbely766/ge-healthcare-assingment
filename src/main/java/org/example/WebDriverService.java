package org.example;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class WebDriverService {


    private static WebDriver webDriver = null;

    public static WebDriver init() {
        if (webDriver == null) {
            ChromeOptions options = new ChromeOptions();
            options.setHeadless(true);
            options.addArguments("--no-sandbox");
            options.addArguments("--disable-dev-shm-usage");
            options.addArguments("--incognito");
            options.addArguments("--window-size=1920,1080");
            webDriver = new ChromeDriver(options);
            return webDriver;
        }
        return webDriver;


    }

    public static void quitWebDriver()
    {
        webDriver.quit();
        webDriver = null;
    }

}
